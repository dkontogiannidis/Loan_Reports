from sqlalchemy import Column, ForeignKey, Integer, String, Float, BLOB, Table, CHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

Base = declarative_base()


class SupportedCurrency(Base):
    """
    Table including all the supported currencies

    """
    __tablename__ = 'supported_currency'

    id = Column(CHAR(3), primary_key=True, nullable=False)
    description = Column(String(255), nullable=False)



class User(Base):
    """
    User Table

    :columns: id, name

    A table of the database represented as an object which includes information about the author of the report.
    """
    __tablename__ = 'user'

    def __init__(self, name):
        self.name = name

    id = Column(Integer, unique=True, autoincrement=True, primary_key=True)
    name = Column(String(255), nullable=False)



class Loan(Base):
    """
    Loan Table

    A table containing information about loans and their balance

    :columns: id, balance, currency
    """
    __tablename__ = 'loan'

    id = Column(Integer, unique=True, autoincrement=True, primary_key=True, nullable=False)
    balance = Column(Float, nullable=False)
    currency = Column(CHAR(3), ForeignKey('supported_currency.id'))
    supported_currency = relationship(SupportedCurrency)
    # Many To Many Relationship with Reports
    reports = relationship(
        'Report',
        secondary='report_loan_link'
    )



class ReportBody(Base):
    """
    ReportBody Table

    Contains the body of the report and is separated because its size can go up to
    5 MB. Therefore, by only referencing the id we avoid slowing down the query when
    the report body is not needed.

    :columns: id, title, body, author_id, loan_id
    """
    __tablename__ = 'report_body'

    id = Column(Integer, unique=True, autoincrement=True, primary_key=True, nullable=False)
    body = Column(BLOB(5242880))



class Report(Base):
    """
    Report Table

    A table containing various report information as well as the authors who issued them.

    :columns: id, title, body_id, author_id
    """
    __tablename__ = 'report'

    id = Column(Integer, unique=True, autoincrement=True, primary_key=True, nullable=False)
    title = Column(String(255), nullable=False)
    body_id = Column(Integer, ForeignKey('report_body.id'))
    author_id = Column(Integer, ForeignKey("user.id"))
    report_body = relationship(ReportBody)
    user = relationship(User)

    # Many To Many Relationship with Loan
    loans = relationship(
        Loan,
        secondary='report_loan_link'
    )



class ReportLoanLink(Base):
    """
    A Many To Many association table to connect multiple reports to multiple Loans
    and vise versa
    """
    __tablename__ = 'report_loan_link'
    report_id = Column(Integer, ForeignKey('report.id'), primary_key=True)
    loan_id = Column(Integer, ForeignKey('loan.id'), primary_key=True)
    report = relationship(Report, backref=backref("loan_assoc"))
    loan = relationship(Loan, backref=backref("report_assoc"))

