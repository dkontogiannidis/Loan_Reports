import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError
import os
from models import Base, User, Report, Loan, SupportedCurrency, ReportLoanLink


class MyTestCase(unittest.TestCase):

    def setUp(self):
        """
        Find the path according to the operating system and create the database to be used by the test cases.
        """
        root_path = os.path.abspath(os.sep)
        new_path = os.path.join(root_path, 'databases')
        if not os.path.exists(new_path):
            os.makedirs(new_path)
        self.db_path = os.path.join(new_path, 'loan_reports.db')
        engine = create_engine('sqlite:///{}'.format(self.db_path), echo=True)
        Base.metadata.create_all(engine)
        self.db_session = sessionmaker()
        self.db_session.configure(bind=engine)

    def tearDown(self):
        """
        Delete all the data in the database and close all sessions if still open.
        """
        self.db_session.close_all()
        if self.db_path:
            os.remove(self.db_path)

    def test_user_column_names_according_to_requirements(self):
        """
        Check if the table has the correct columns
        """
        user_columns = [column.key.lower() for column in User.__table__.columns]
        self.assertItemsEqual(['id', 'name'], user_columns)

    def test_report_column_names_according_to_requirements(self):
        """
        Check if the table has the correct columns
        """
        report_columns = [column.key.lower() for column in Report.__table__.columns]
        self.assertItemsEqual(['id', 'title', 'body_id', 'author_id'], report_columns)

    def test_loan_column_names_according_to_requirements(self):
        """
        Check if the table has the correct columns
        """
        loan_columns = [column.key.lower() for column in Loan.__table__.columns]
        self.assertItemsEqual(['id', 'balance', 'currency'], loan_columns)

    def test_supported_currencies_column_names_according_to_requirements(self):
        """
        Check if the table has the correct columns
        """
        sc_columns = [column.key.lower() for column in SupportedCurrency.__table__.columns]
        self.assertItemsEqual(['id', 'description'], sc_columns)

    def test_user_name_length_constraints(self):
        """
        Respect the maximum length (255) of name column
        """
        long_name = ''.join(str(e) for e in range(300))
        user = User(long_name)
        session = self.db_session()
        session.add(user)
        self.assertRaises(IntegrityError, session.commit())
        session.close()

    def test_not_nullable_user_name(self):
        """
        Respect the not null constraint of the name in the User table
        """
        unique_user = User(name="David Tester")
        non_unique_user = User(name="David Tester")
        session = self.db_session()
        session.add(unique_user)
        session.add(non_unique_user)
        self.assertRaises(IntegrityError, session.commit())
        session.close()

    # TODO: Must be implemented in application level
    #
    # def test_loan_value_always_greater_than_zero(self):
    #     """
    #     Respect the grater than or equal to zero constraint for the loan balance.
    #     """
    #     loan = Loan(balance=-1.0)
    #     session = self.db_session()
    #     session.add(loan)
    #     self.assertRaises(IntegrityError, session.commit())
    #     session.close()

    def test_loan_value_accepts_double(self):
        """
        Respect the grater than or equal to zero constraint for the loan balance.
        """
        loan = Loan()
        loan.balance = 1.0
        session = self.db_session()
        session.add(loan)
        self.assertEqual(1.0, session.query(Loan).filter(Loan.balance == 1.0).all()[0].balance)
        session.close()

    def test_not_nullable_report_title(self):
        """
        Respect the not null constraint of the title in the Report table
        """
        unique_report = Report()
        unique_report.title = "Loan Report"
        non_unique_report = Report()
        non_unique_report.title = "Loan Report"
        session = self.db_session()
        session.add(unique_report)
        session.add(non_unique_report)
        self.assertRaises(IntegrityError, session.commit())
        session.close()

    def test_acceptance_test(self):
        """
        Insert some data in the database and check if they can be queried correctly
        """
        # Insert Data
        user = User("David Tester")
        currency = SupportedCurrency()
        currency.id = "USD"
        currency.description = "United States Dollar"
        loan = Loan()
        loan.balance = 1000.0
        loan.currency = "USD"
        session = self.db_session()
        session.add(user)
        session.add(currency)
        session.add(loan)
        session.commit()
        report = Report()
        report.title = "Title"
        report.author_id = session.query(User).all()[0].id
        session.add(report)
        session.commit()
        report_loan_link = ReportLoanLink()
        report_loan_link.loan_id = session.query(Loan).all()[0].id
        report_loan_link.report_id = session.query(Report).all()[0].id
        session.add(report_loan_link)
        session.commit()

        # Query to retrieve inserted data and check if are stored correctly
        report = session.query(Report).filter(report.title == "Title").all()[0]
        self.assertEqual([1000.0], [report_loan.balance for report_loan in report.loans])
        self.assertEqual(["USD"], [report_loan.currency for report_loan in report.loans])
        self.assertEqual("David Tester", session.query(User).filter(user.id == report.author_id).all()[0].name)

    def test_all_loans_exist_in_report_and_total_balance_is_correct(self):

        expected_report_balance = 0.0
        first_loan_expected_id = 0
        second_loan_expected_id = 0

        # Insert Data
        session = self.db_session()
        # Add a user to the session
        user = User("David Tester")
        session.add(user)
        # Add a supported type of currency
        currency = SupportedCurrency()
        currency.id = "GBP"
        currency.description = "Great Britain Pound Sterling"
        session.add(currency)
        # Add 2 loans to the session
        loan = Loan()
        loan.balance = 1000.0
        expected_report_balance += loan.balance
        loan.currency = "GBP"
        session.add(loan)
        loan = Loan()
        loan.balance = 1500.0
        expected_report_balance += loan.balance
        loan.currency = "GBP"
        session.add(loan)
        # Apply changes stored in session to database
        session.commit()

        # Create and add a report to session
        report = Report()
        report.title = "Title"
        report.author_id = session.query(User).all()[0].id
        session.add(report)
        # Apply changes to database
        session.commit()

        # Link both of the loans with only one report
        report_loan_link = ReportLoanLink()
        # The first loan
        loans = session.query(Loan).all()
        report_loan_link.loan_id = loans[0].id
        first_loan_expected_id = loans[0].id
        report_loan_link.report_id = session.query(Report).all()[0].id
        # Add first loan to report connection
        session.add(report_loan_link)
        report_loan_link = ReportLoanLink()
        # Add second loan to report connection
        report_loan_link.loan_id = loans[1].id
        second_loan_expected_id = loans[1].id
        # The same Report as above
        report_loan_link.report_id = session.query(Report).all()[0].id
        session.add(report_loan_link)
        # Appply changes to database
        session.commit()

        # Query to retrieve inserted data and check if are stored correctly
        report = session.query(Report).filter(report.title == "Title").all()[0]
        # Check if both loans exist
        self.assertItemsEqual([first_loan_expected_id, second_loan_expected_id], [loan.id for loan in report.loans])
        # Check if the loan balance is as expected
        self.assertEqual(expected_report_balance, sum([loan.balance for loan in list(report.loans)]))






if __name__ == '__main__':
    unittest.main()
